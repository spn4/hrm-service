<?php

namespace SpondonIt\HrmService;

use Illuminate\Support\ServiceProvider;
use Illuminate\Contracts\Http\Kernel;
use SpondonIt\HrmService\Middleware\HrmService;

class SpondonItHrmServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        $kernel = $this->app->make(Kernel::class);
        $kernel->pushMiddleware(HrmService::class);

        $this->loadRoutesFrom(__DIR__.'/../routes/web.php');
        $this->loadTranslationsFrom(__DIR__.'/../resources/lang', 'hrm');
        $this->loadViewsFrom(__DIR__.'/../resources/views', 'hrm');
        $this->loadMigrationsFrom(__DIR__.'/../database/migrations');
    }
}
