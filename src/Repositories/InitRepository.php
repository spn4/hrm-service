<?php

namespace SpondonIt\HrmService\Repositories;

use App\User;

use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Storage;
use Modules\GeneralSetting\Entities\GeneralSetting;
use Modules\Notification\Entities\Notification;
use Modules\RolePermission\Entities\Role;
use Nwidart\Modules\Facades\Module;
use Spatie\Valuestore\Valuestore;

class InitRepository
{


    public function init()
    {
        config([
            'app.item' => '3841010',
            'spondonit.module_manager_model' => \Modules\ModuleManager\Entities\InfixModuleManager::class,
            'spondonit.module_manager_table' => 'infix_module_managers',

            'spondonit.settings_model' => GeneralSetting::class,
            'spondonit.module_model' => Module::class,

            'spondonit.user_model' => User::class,
            'spondonit.settings_table' => 'general_settings',
            'spondonit.database_file' => '',
            'spondonit.support_multi_connection' => false
        ]);
    }

    public function config()
    {

        if (Storage::has('settings.json')) {
            app()->singleton('permission_list', function () {
                return Cache::rememberForever('permission_list', function () {
                    return Role::with(['permissions' => function ($query) {
                        $query->select('route', 'module_id', 'parent_id', 'role_permission.role_id');
                    }])->get(['id', 'name']);
                });
            });



            view()->composer('backEnd.partials.menu', function ($view) {
                $data = [
                    'specific_notifications' => Notification::with('notifiable')->where('user_id', auth()->id())
                        ->where('role', auth()->user()->role_id)->where('read_at', null)->latest()->get(),

                    'notifications' => Notification::with('notifiable')->where('read_at', null)->where('user_id', null)
                        ->where(function ($query) {
                            $query->where('role', null)->orWhere('role', 0);
                        })->latest()->get(),
                ];
                $view->with($data);
            });
        }
    }

}
